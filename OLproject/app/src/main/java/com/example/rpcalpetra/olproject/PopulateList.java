package com.example.rpcalpetra.olproject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rpcalpetra on 16/06/16.
 */
public class PopulateList {
    private static PopulateList sInstance;

    private PopulateList() {}

    public static PopulateList getInstance() {
        if (sInstance == null) {
            sInstance = new PopulateList();
        }

        return sInstance;
    }


    public List<ItemsList> getItems() {
        List<ItemsList> items = new ArrayList<>(6);

        items.add(new ItemsList("Carros", R.drawable.olx_icon));
        items.add(new ItemsList("Motas", R.drawable.olx_icon));
        items.add(new ItemsList("Bicicletas",R.drawable.olx_icon));
        items.add(new ItemsList("Computadores", R.drawable.olx_icon));
        items.add(new ItemsList("Telemoveis", R.drawable.olx_icon));
        items.add(new ItemsList("Casas", R.drawable.olx_icon));

        return items;
    }
}

