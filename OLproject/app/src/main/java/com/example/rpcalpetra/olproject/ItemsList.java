package com.example.rpcalpetra.olproject;

/**
 * Created by rpcalpetra on 16/06/16.
 */
public class ItemsList {

        private String title;
        private int iconPath;


        public ItemsList(String title, int iconPath) {
            this.title = title;
            this.iconPath = iconPath;

        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getIconPath() {
            return iconPath;
        }

        public void setIconPath(int iconPath) {
            this.iconPath = iconPath;
        }



}
