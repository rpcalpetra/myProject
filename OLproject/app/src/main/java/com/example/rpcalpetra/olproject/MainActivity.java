package com.example.rpcalpetra.olproject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


public class MainActivity extends Activity {
    RecyclerView rvLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvLista = (RecyclerView) findViewById(R.id.rv_lista);
        rvLista.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rvLista.setLayoutManager(llm);

        AdapterLista adapter = new AdapterLista(getApplicationContext(),
                PopulateList.getInstance().getItems());
        rvLista.setAdapter(adapter);

    }

}
