package com.example.rpcalpetra.olproject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by rpcalpetra on 16/06/16.
 */
public class AdapterLista extends RecyclerView.Adapter<AdapterLista.ViewHolder> {
    private Context mContext;
    private List<ItemsList> mItems;
    private OnItemClick mListener;

    public AdapterLista(Context context, List<ItemsList> items) {
        this.mContext = context;
        this.mItems = items;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mIcon;
        TextView mTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            mIcon = (ImageView) itemView.findViewById(R.id.iv_icon);
            mTitle = (TextView) itemView.findViewById(R.id.tv_title);

            itemView.findViewById(R.id.rl_list_item).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
           //TODO set click action
            }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View inflated = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(inflated);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        viewHolder.mIcon.setImageResource(mItems.get(i).getIconPath());
        viewHolder.mTitle.setText(mItems.get(i).getTitle());
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnItemClick {

    }
}
